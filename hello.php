<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calcolatrice PHP con tastiera, utente 1</title>
    <style>
        .calculator {
            border: 1px solid #ccc;
            padding: 20px;
            width: 300px;
            text-align: center;
            margin: 0 auto;
        }
        .button {
            width: 50px;
            height: 50px;
            font-size: 18px;
            margin: 5px;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="calculator">
        <h2>Calcolatrice PHP</h2>

        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            <input type="text" name="display" id="display" readonly style="width: 250px; height: 40px; font-size: 18px; margin-bottom: 10px;"><br>

            <input type="hidden" name="num1" id="num1">
            <input type="hidden" name="operation" id="operation">

            <div>
                <button class="button" type="button" onclick="addToDisplay('7')">7</button>
                <button class="button" type="button" onclick="addToDisplay('8')">8</button>
                <button class="button" type="button" onclick="addToDisplay('9')">9</button>
                <button class="button" type="button" onclick="setOperation('+')">+</button>
                <br>
                <button class="button" type="button" onclick="addToDisplay('4')">4</button>
                <button class="button" type="button" onclick="addToDisplay('5')">5</button>
                <button class="button" type="button" onclick="addToDisplay('6')">6</button>
                <button class="button" type="button" onclick="setOperation('-')">-</button>
                <br>
                <button class="button" type="button" onclick="addToDisplay('1')">1</button>
                <button class="button" type="button" onclick="addToDisplay('2')">2</button>
                <button class="button" type="button" onclick="addToDisplay('3')">3</button>
                <button class="button" type="button" onclick="setOperation('*')">*</button>
                <br>
                <button class="button" type="button" onclick="addToDisplay('0')">0</button>
                <button class="button" type="button" onclick="addToDisplay('.')">.</button>
                <button class="button" type="button" onclick="clearDisplay()">C</button>
                <button class="button" type="button" onclick="setOperation('/')">/</button>
                <br><br>
                <input type="submit" name="calcola" value="Calcola" style="width: 250px; height: 40px; font-size: 18px;">
            </div>
        </form>

        <?php
        // Funzione JavaScript per aggiungere numeri e punti al display
        echo '<script>
            function addToDisplay(value) {
                document.getElementById("display").value += value;
            }
            // Funzione JavaScript per cancellare il display
            function clearDisplay() {
                document.getElementById("display").value = "";
            }
            // Funzione JavaScript per impostare l operazione
            function setOperation(operation) {
                document.getElementById("num1").value = document.getElementById("display").value;
                document.getElementById("operation").value = operation;
                clearDisplay();
            }
        </script>';

        // Verifica se il modulo è stato inviato
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Ottieni i valori inseriti dall'utente
            $num1 = $_POST['num1'];
            $num2 = $_POST['display']; // Il secondo numero è quello mostrato sul display
            $operazione = $_POST['operation'];

            // Esegui l'operazione selezionata
            switch ($operazione) {
                case '+':
                    $risultato = $num1 + $num2;
                    break;
                case '-':
                    $risultato = $num1 - $num2;
                    break;
                case '*':
                    $risultato = $num1 * $num2;
                    break;
                case '/':
                    if ($num2 != 0) {
                        $risultato = $num1 / $num2;
                    } else {
                        echo "Impossibile dividere per zero!";
                        exit;
                    }
                    break;
                default:
                    echo "Operazione non valida";
                    exit;
            }

            // Stampare il risultato
            echo "<h3>Risultato: $risultato</h3>";
        }
        ?>
    </div>
</body>
</html>
